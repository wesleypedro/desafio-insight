package br.ufc.insight.desafiodevops.service.Implements;

import br.ufc.insight.desafiodevops.exception.DataIntegrity;
import br.ufc.insight.desafiodevops.exception.NotFound;
import br.ufc.insight.desafiodevops.model.*;
import br.ufc.insight.desafiodevops.repository.TaskRepository;
import br.ufc.insight.desafiodevops.service.AdministratorService;
import br.ufc.insight.desafiodevops.service.ServerService;
import br.ufc.insight.desafiodevops.service.TaskService;
import br.ufc.insight.desafiodevops.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskServiceImplement implements TaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ServerService serverService;

    @Autowired
    private AdministratorService administratorService;

    @Override
    public Task save(Task task) {

        User user = userService.getLogged();

        if (!user.getPapeis().contains(Papel.ADMINISTRADOR)) {
            throw new DataIntegrity("Erro! O usuário não pode criar uma tarefa!");
        }

        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("Deadline: " + task.getDeadline());
        System.out.println("Priority: " + task.getTaskPriority());
        System.out.println("Responsible: " + task.getResponsible().getUser().getName());

        if (task.getName() == null
            || task.getName().trim().equals("")
            || task.getDescription() == null
            || task.getDescription().trim().equals("")
            || task.getDeadline() == null
            || task.getTaskPriority() == null
            || task.getResponsible() == null) {

            throw new DataIntegrity("Erro! Certifique-se de que todos os campos estão preenchidos!");
        }

        Server server = serverService.getOne(task.getResponsible().getId());

        if (server == null) {
            throw new NotFound("Erro! Servidor responsável pela tarefa não foi encontrado!");
        } else if (!server.getUser().getPapeis().contains(Papel.SERVIDOR)) {
            throw new DataIntegrity("Erro! Servidor não pode ser atribuído como responsável pela tarefa!");
        }

        Administrator administrator = administratorService.getByUser(user);

        task.setTaskStatus(TaskStatus.ABERTA);
        task.setRequester(administrator);

        return taskRepository.save(task);
    }

    @Override
    public List<Task> getAll() {
        User user = userService.getLogged();

        List<Task> tasks = taskRepository.findAll();
        List<Task> tasks1 = new ArrayList<>();

        for (Task task : tasks) {
            if (task.getRequester().getUser().equals(user) || task.getResponsible().getUser().equals(user)) {
                tasks1.add(task);
            }
        }

        return tasks1;
    }

    @Override
    public Task update(Integer id, Task task) {
        Task task1 = taskRepository.findById(id).orElse(null);

        if (task1 == null) {
            throw new NotFound("Erro! Tarefa não encontrada!");
        }

        User user = userService.getLogged();

        if (!user.getPapeis().contains(Papel.ADMINISTRADOR) || !task1.getRequester().getUser().equals(user)) {
            throw new DataIntegrity("Erro! Usuário não autorizado a alterar esta tarefa!");
        }

        if (task.getName() == null
            || task.getName().trim().equals("")
            || task.getDescription() == null
            || task.getDescription().trim().equals("")
            || task.getDeadline() == null
            || task.getTaskPriority() == null
            || task.getResponsible() == null) {

            throw new DataIntegrity("Erro! Certifique-se de que todos os campos estão preenchidos!");
        }

        if (task1.getTaskStatus().equals(TaskStatus.ANDAMENTO)) {
            throw new DataIntegrity("Erro! Certifique se de que a tarefa não está em andamento!");
        }

        Server server = serverService.getOne(task.getResponsible().getId());

        if (server == null) {
            throw new DataIntegrity("Erro! Servidor responsável pela tarefa não foi encontrado!");
        } else if (!server.getUser().getPapeis().contains(Papel.SERVIDOR)) {
            throw new DataIntegrity("Erro! Servidor não pode ser atribuído como responsável pela tarefa!");
        }

        task1.setName(task.getName());
        task1.setDescription(task.getDescription());
        task1.setDeadline(task.getDeadline());
        task1.setTaskPriority(task.getTaskPriority());
        task1.setResponsible(task.getResponsible());

        task1.setTaskStatus(TaskStatus.ABERTA);

        return taskRepository.save(task1);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
    }

    @Override
    public void delete(Integer id) {
        Task task = taskRepository.findById(id).orElse(null);

        if (task == null) {
            throw new NotFound("Erro! Certifique-se de que a informação passada é válida");
        }

        if (task.getRequester().getUser().equals(userService.getLogged())) {
            if (task.getTaskStatus().equals(TaskStatus.ANDAMENTO)) {
                throw new DataIntegrity("Erro! A tarefa está com  em andamento!");
            }
            taskRepository.delete(task);
        } else {
            throw new DataIntegrity("Erro! A tarefa não pertence a este usuário");
        }
    }

    @Override
    public Task getOne(Integer id) {
        Task task = taskRepository.findById(id).orElse(null);

        if (task == null) {
            throw new NotFound("Erro! Tarefa não encontrada!");
        }

        User user = userService.getLogged();

        if (task.getRequester().getUser().equals(user) || task.getResponsible().getUser().equals(user)) {
            return task;
        }

        throw new DataIntegrity("Erro! Tarefa não contém associação com este usuário!");
    }

    @Override
    public Task updateStatusProgress(Integer id) {
        Task task = taskRepository.findById(id).orElse(null);

        if (task == null) {
            throw new NotFound("Erro! Tarefa não encontrada!");
        }

        User user = userService.getLogged();

        if (!task.getResponsible().getUser().equals(user)) {
           throw new DataIntegrity("Erro! Tarefa não pode ser modificada por este usuário!");
        }

        task.setTaskStatus(TaskStatus.ANDAMENTO);

        return taskRepository.save(task);
    }

    @Override
    public Task updateStatusFinished(Integer id) {
        Task task = taskRepository.findById(id).orElse(null);

        if (task == null) {
            throw new NotFound("Erro! Tarefa não encontrada!");
        }

        User user = userService.getLogged();

        if (!task.getResponsible().getUser().equals(user)) {
            throw new DataIntegrity("Erro! Tarefa não pode ser modificada por este usuário!");
        }

        task.setTaskStatus(TaskStatus.FINALIZADA);

        return taskRepository.save(task);
    }
}
