package br.ufc.insight.desafiodevops.service;

import br.ufc.insight.desafiodevops.model.Server;
import br.ufc.insight.desafiodevops.model.User;

import java.util.List;

public interface ServerService {
    Server save(Server server);

    List<Server> getAll();

    Server update(Integer id, Server server);

    void delete(Integer id);

    Server getOne(Integer id);

    Server getByUser(User user);
}
