package br.ufc.insight.desafiodevops.repository;

import br.ufc.insight.desafiodevops.model.Server;
import br.ufc.insight.desafiodevops.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServerRepository extends JpaRepository<Server, Integer> {
    Server findByServerIdentification(String serverIdentification);

    Server findByUser(User user);
}
