package br.ufc.insight.desafiodevops.exception;

public class NotFound extends RuntimeException {

    public NotFound(String message) {
        super(message);
    }
}
