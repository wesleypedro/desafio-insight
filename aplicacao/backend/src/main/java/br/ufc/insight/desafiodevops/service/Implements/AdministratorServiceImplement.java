package br.ufc.insight.desafiodevops.service.Implements;

import br.ufc.insight.desafiodevops.exception.DataIntegrity;
import br.ufc.insight.desafiodevops.model.Administrator;
import br.ufc.insight.desafiodevops.model.User;
import br.ufc.insight.desafiodevops.repository.AdministratorRepository;
import br.ufc.insight.desafiodevops.service.AdministratorService;
import br.ufc.insight.desafiodevops.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdministratorServiceImplement implements AdministratorService {

    @Autowired
    private AdministratorRepository administratorRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Administrator save(Administrator administrator) {
        if (administratorRepository.findByBoardMemberCode(administrator.getBoardMemberCode()) != null) {
            throw new DataIntegrity("Erro! Código de membro do conselho já existe!");
        }

        User user = userService.getByEmail(administrator.getUser().getEmail());
        if (user != null) {
//            throw new DataIntegrity("Erro! Email já cadastrado!");
            administrator.setUser(user);
        } else {
            String password = passwordEncoder.encode(administrator.getUser().getPassword());
            administrator.getUser().setPassword(password);
        }

        return administratorRepository.save(administrator);
    }

    @Override
    public List<Administrator> getAll() {
        return null;
    }

    @Override
    public Administrator update(Integer id, Administrator administrator) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Administrator getOne(Integer id) {
        return administratorRepository.getOne(id);
    }

    @Override
    public Administrator getByUser(User user) {
        return administratorRepository.findByUser(user);
    }
}
