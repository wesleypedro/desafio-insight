package br.ufc.insight.desafiodevops.controller;

import br.ufc.insight.desafiodevops.model.Papel;
import br.ufc.insight.desafiodevops.model.User;
import br.ufc.insight.desafiodevops.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<User> save(@RequestBody User user) {
//        return ResponseEntity.ok(userService.save(user));
        return null;
    }

    @GetMapping
    public ResponseEntity<List<User>> getAll() {
//        return ResponseEntity.ok(userService.getAll());
        return null;
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> update(@PathVariable Integer id, @RequestBody User user) {
//        return ResponseEntity.ok(userService.update(id, user));
        return null;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
//        return ResponseEntity.ok(userService.delete(id));
        return null;
    }

    //Get One
    @GetMapping("/{id}")
    public ResponseEntity<User> getOne(@PathVariable Integer id) {
//        return ResponseEntity.ok(userService.getOne(id));
        return null;
    }

    @GetMapping("/papeis")
    public ResponseEntity<List<Papel>> getPapeis() {
        return ResponseEntity.ok(userService.getPapeis());
    }
}
