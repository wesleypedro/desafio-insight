package br.ufc.insight.desafiodevops.exception;

import org.springframework.security.core.AuthenticationException;

public class InvalidJwtAuthentication extends AuthenticationException {

    private static final long serialVersionUID = 1L;

    public InvalidJwtAuthentication(String e) {
        super(e);
    }

}

