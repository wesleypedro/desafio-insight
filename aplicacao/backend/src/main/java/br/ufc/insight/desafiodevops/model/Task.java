package br.ufc.insight.desafiodevops.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "task")
public class Task implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private String description;

    private Date deadline;

    @Enumerated(EnumType.STRING)
    @Column(name = "task_status")
    private TaskStatus taskStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "task_priority")
    private TaskPriority taskPriority;

    @ManyToOne
    @JoinColumn(name = "administrator_id")
    private Administrator requester;

    @ManyToOne
    @JoinColumn(name = "server_id")
    private Server responsible;

    public Task() { }

    public Task(Integer id, String name, String description, Date deadline, TaskStatus taskStatus, TaskPriority taskPriority, Administrator requester, Server responsible) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.deadline = deadline;
        this.taskStatus = taskStatus;
        this.taskPriority = taskPriority;
        this.requester = requester;
        this.responsible = responsible;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public TaskStatus getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(TaskStatus taskStatus) {
        this.taskStatus = taskStatus;
    }

    public TaskPriority getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(TaskPriority taskPriority) {
        this.taskPriority = taskPriority;
    }

    public Administrator getRequester() {
        return requester;
    }

    public void setRequester(Administrator requester) {
        this.requester = requester;
    }

    public Server getResponsible() {
        return responsible;
    }

    public void setResponsible(Server responsible) {
        this.responsible = responsible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id.equals(task.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
