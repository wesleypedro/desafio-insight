package br.ufc.insight.desafiodevops.service;

import br.ufc.insight.desafiodevops.model.Task;

import java.util.List;

public interface TaskService {
    Task save(Task task);

    List<Task> getAll();

    Task update(Integer id, Task task);

    void delete(Integer id);

    Task getOne(Integer id);

    Task updateStatusProgress(Integer id);

    Task updateStatusFinished(Integer id);
}
