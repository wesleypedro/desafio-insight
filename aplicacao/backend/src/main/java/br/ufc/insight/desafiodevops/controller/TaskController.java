package br.ufc.insight.desafiodevops.controller;

import br.ufc.insight.desafiodevops.model.Task;
import br.ufc.insight.desafiodevops.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private TaskService taskService;

    @PostMapping
    public ResponseEntity<Task> save(@RequestBody Task task) {
        return ResponseEntity.ok(taskService.save(task));
    }

    @GetMapping
    public ResponseEntity<List<Task>> getAll() {
        return ResponseEntity.ok(taskService.getAll());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Task> update(@PathVariable Integer id, @RequestBody Task task) {
        return ResponseEntity.ok(taskService.update(id, task));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
        taskService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Task> getOne(@PathVariable Integer id) {
        return ResponseEntity.ok(taskService.getOne(id));
    }

    @PutMapping("/progress/{id}")
    public ResponseEntity<Task> updateStatusProgress(@PathVariable Integer id) {
        return ResponseEntity.ok(taskService.updateStatusProgress(id));
    }

    @PutMapping("/finished/{id}")
    public ResponseEntity<Task> updateStatusFinished(@PathVariable Integer id) {
        return ResponseEntity.ok(taskService.updateStatusFinished(id));
    }
}
