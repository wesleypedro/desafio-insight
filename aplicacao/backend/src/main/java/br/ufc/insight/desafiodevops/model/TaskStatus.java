package br.ufc.insight.desafiodevops.model;

public enum TaskStatus {
    ABERTA("Em aberto"),
    ANDAMENTO("Em andamento"),
    FINALIZADA("Finalizada");

    private String description;

    TaskStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.getDescription();
    }
}
