package br.ufc.insight.desafiodevops.model;

public enum TaskPriority {
    BAIXA("Prioridade baixa"),
    MEDIA("Prioridade média"),
    ALTA("Prioridade alta");

    private String description;

    TaskPriority(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.getDescription();
    }
}
