package br.ufc.insight.desafiodevops.controller;

import br.ufc.insight.desafiodevops.model.Administrator;
import br.ufc.insight.desafiodevops.service.AdministratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdministratorController {

    @Autowired
    private AdministratorService administratorService;

    @PostMapping
    public ResponseEntity<Administrator> save(@RequestBody Administrator administrator) {
//        return ResponseEntity.ok(administratorService.save(administrator));
        return null;
    }

    @GetMapping
    public ResponseEntity<List<Administrator>> getAll() {
//        return ResponseEntity.ok(administratorService.getAll());
        return null;
    }

    @PutMapping("/{id}")
    public ResponseEntity<Administrator> update(@PathVariable Integer id, @RequestBody Administrator administrator) {
//        return ResponseEntity.ok(administratorService.update(id, administrator));
        return null;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
//        administratorService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Administrator> getOne(@PathVariable Integer id) {
//        return ResponseEntity.ok(administratorService.getOne(id));
        return null;
    }
}
