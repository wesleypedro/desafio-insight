package br.ufc.insight.desafiodevops.service.Implements;

import br.ufc.insight.desafiodevops.exception.DataIntegrity;
import br.ufc.insight.desafiodevops.model.Papel;
import br.ufc.insight.desafiodevops.model.User;
import br.ufc.insight.desafiodevops.repository.UserRepository;
import br.ufc.insight.desafiodevops.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImplement implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User save(User user) {
        if (getByEmail(user.getEmail()) != null) {
            throw new DataIntegrity("Erro! Email já cadastrado!");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    @Override
    public List<User> getAll() {
        return null;
    }

    @Override
    public User update(Integer id, User user) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User getOne(Integer id) {
        return null;
    }

    @Override
    public List<Papel> getPapeis() {
        User user = this.getLogged();
        return user.getPapeis();
    }

    @Override
    public User getLogged() {
        Object loggedUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String email = ((UserDetails)loggedUser).getUsername();
        return userRepository.findByEmail(email);
    }
}
