package br.ufc.insight.desafiodevops.repository;

import br.ufc.insight.desafiodevops.model.Administrator;
import br.ufc.insight.desafiodevops.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {
    Administrator findByBoardMemberCode(String boardMemberCode);

    Administrator findByUser(User user);
}
