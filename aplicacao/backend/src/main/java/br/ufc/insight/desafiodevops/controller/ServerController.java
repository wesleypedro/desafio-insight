package br.ufc.insight.desafiodevops.controller;

import br.ufc.insight.desafiodevops.model.Server;
import br.ufc.insight.desafiodevops.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/server")
public class ServerController {

    @Autowired
    private ServerService serverService;

    @PostMapping
    public ResponseEntity<Server> save(@RequestBody Server server) {
//        return ResponseEntity.ok(serverService.save(server));
        return null;
    }

    @GetMapping
    public ResponseEntity<List<Server>> getAll() {
        return ResponseEntity.ok(serverService.getAll());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Server> update(@PathVariable Integer id, @RequestBody Server server) {
//        return ResponseEntity.ok(serverService.update(id, server));
        return null;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id) {
//        return ResponseEntity.ok(serverService.delete(id));
        return null;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Server> getOne(@PathVariable Integer id) {
//        return ResponseEntity.ok(serverService.getOne(id));
        return null;
    }
}
