package br.ufc.insight.desafiodevops.service;

import br.ufc.insight.desafiodevops.model.Papel;
import br.ufc.insight.desafiodevops.model.User;

import java.util.List;

public interface UserService {
    User save(User user);

    List<User> getAll();

    User update(Integer id, User user);

    void delete(Integer id);

    User getByEmail(String email);

    User getOne(Integer id);

    List<Papel> getPapeis();

    User getLogged();
}
