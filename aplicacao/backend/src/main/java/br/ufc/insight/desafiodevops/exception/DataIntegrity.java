package br.ufc.insight.desafiodevops.exception;

public class DataIntegrity extends RuntimeException {

    public DataIntegrity(String message) {
        super(message);
    }
}
