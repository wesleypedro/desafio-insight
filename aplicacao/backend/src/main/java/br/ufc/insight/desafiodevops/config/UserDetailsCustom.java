package br.ufc.insight.desafiodevops.config;

import br.ufc.insight.desafiodevops.model.User;
import br.ufc.insight.desafiodevops.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsCustom implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) {
		User user = userService.getByEmail(username);

		if (user == null) {
			throw new UsernameNotFoundException("Usuário e/ou senha inválidos");
		} else {
			return user;
		}
	}
}
