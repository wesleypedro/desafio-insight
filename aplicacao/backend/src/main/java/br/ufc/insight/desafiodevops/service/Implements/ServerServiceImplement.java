package br.ufc.insight.desafiodevops.service.Implements;

import br.ufc.insight.desafiodevops.model.Server;
import br.ufc.insight.desafiodevops.model.User;
import br.ufc.insight.desafiodevops.repository.ServerRepository;
import br.ufc.insight.desafiodevops.service.ServerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServerServiceImplement implements ServerService {

    @Autowired
    private ServerRepository serverRepository;


    @Override
    public Server save(Server server) {
        return null;
    }

    @Override
    public List<Server> getAll() {
        return serverRepository.findAll();
    }

    @Override
    public Server update(Integer id, Server server) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Server getOne(Integer id) {
        return serverRepository.getOne(id);
    }

    @Override
    public Server getByUser(User user) {
        return null;
    }
}
