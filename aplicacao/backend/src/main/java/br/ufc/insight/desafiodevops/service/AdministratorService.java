package br.ufc.insight.desafiodevops.service;

import br.ufc.insight.desafiodevops.model.Administrator;
import br.ufc.insight.desafiodevops.model.User;

import java.util.List;

public interface AdministratorService {
    Administrator save(Administrator administrator);

    List<Administrator> getAll();

    Administrator update(Integer id, Administrator administrator);

    void delete(Integer id);

    Administrator getOne(Integer id);

    Administrator getByUser(User user);
}
