package br.ufc.insight.desafiodevops.model;

import org.springframework.security.core.GrantedAuthority;

public enum Papel implements GrantedAuthority {
    SERVIDOR("Servidor"),
    ADMINISTRADOR("Administrador");

    private String description;

    Papel(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.getDescription();
    }

    @Override
    public String getAuthority() {
        return this.toString();
    }
}
