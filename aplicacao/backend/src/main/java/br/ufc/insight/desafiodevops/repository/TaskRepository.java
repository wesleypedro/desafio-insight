package br.ufc.insight.desafiodevops.repository;

import br.ufc.insight.desafiodevops.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task, Integer> {

}
