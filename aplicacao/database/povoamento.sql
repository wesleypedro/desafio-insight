insert into users (name, email, password, enabled) values
	('administrator 1', 'administrator1@mail.com', '$2a$10$jtiVCtavXnrvAtidA0koN.3roWbOKTZOcq1/AvNr86hliRxjCbMfy', true),
	('administrator 2', 'administrator2@mail.com', '$2a$10$jtiVCtavXnrvAtidA0koN.3roWbOKTZOcq1/AvNr86hliRxjCbMfy', true),
	('administrator 3', 'administrator3@mail.com', '$2a$10$jtiVCtavXnrvAtidA0koN.3roWbOKTZOcq1/AvNr86hliRxjCbMfy', true),
	('Server 1', 'server1@mail.com', '$2a$10$jtiVCtavXnrvAtidA0koN.3roWbOKTZOcq1/AvNr86hliRxjCbMfy', true),
	('Server 2', 'server2@mail.com', '$2a$10$jtiVCtavXnrvAtidA0koN.3roWbOKTZOcq1/AvNr86hliRxjCbMfy', true),
	('Server 3', 'server3@mail.com', '$2a$10$jtiVCtavXnrvAtidA0koN.3roWbOKTZOcq1/AvNr86hliRxjCbMfy', true),
	('Server 4', 'server4@mail.com', '$2a$10$jtiVCtavXnrvAtidA0koN.3roWbOKTZOcq1/AvNr86hliRxjCbMfy', true),
	('Server 5', 'server5@mail.com', '$2a$10$jtiVCtavXnrvAtidA0koN.3roWbOKTZOcq1/AvNr86hliRxjCbMfy', true);


insert into papel_user (user_id, papel) values 
	(1, 'ADMINISTRADOR'),
	(2, 'ADMINISTRADOR'),
	(3, 'ADMINISTRADOR'),
	(4, 'SERVIDOR'),
	(5, 'SERVIDOR'),
	(6, 'SERVIDOR'),
	(7, 'SERVIDOR'),
	(8, 'SERVIDOR');


insert into administrator (board_member_code, user_id) values 
	('123', 1),
	('456', 2),
	('789', 3);


insert into server (server_identification, user_id) values 
	('234', 4), 
	('345', 5), 
	('567', 6), 
	('678', 7), 
	('890', 8); 


insert into task (name, description, deadline, task_status, task_priority, administrator_id, server_id) values 
	('task ', 'description ', '2021-01-10', 'FINALIZADA', 'ALTA', 1, 1),
	('task ', 'description ', '2021-01-15', 'ANDAMENTO', 'MEDIA', 3, 2),
	('task ', 'description ', '2021-01-11', 'ABERTA', 'BAIXA', 2, 3),
	('task ', 'description ', '2021-01-11', 'FINALIZADA', 'ALTA', 1, 4),
	('task ', 'description ', '2021-01-12', 'ANDAMENTO', 'MEDIA', 3, 5),
	('task ', 'description ', '2021-01-12', 'ABERTA', 'BAIXA', 2, 1),
	('task ', 'description ', '2021-01-12', 'FINALIZADA', 'ALTA', 1, 2),
	('task ', 'description ', '2021-01-13', 'ANDAMENTO', 'MEDIA', 3, 3),
	('task ', 'description ', '2021-01-14', 'ABERTA', 'BAIXA', 2, 4),
	('task ', 'description ', '2021-01-14', 'FINALIZADA', 'ALTA', 1, 5),
	('task ', 'description ', '2021-01-13', 'ANDAMENTO', 'MEDIA', 3, 1),
	('task ', 'description ', '2021-01-11', 'ABERTA', 'BAIXA', 2, 2),
	('task ', 'description ', '2021-01-15', 'FINALIZADA', 'ALTA', 1, 3),
	('task ', 'description ', '2021-01-14', 'ANDAMENTO', 'MEDIA', 3, 4),
	('task ', 'description ', '2021-01-15', 'ABERTA', 'BAIXA', 2, 5),
	('task ', 'description ', '2021-01-12', 'FINALIZADA', 'ALTA', 1, 1),
	('task ', 'description ', '2021-01-13', 'ANDAMENTO', 'MEDIA', 3, 2),
	('task ', 'description ', '2021-01-12', 'ABERTA', 'BAIXA', 2, 3);