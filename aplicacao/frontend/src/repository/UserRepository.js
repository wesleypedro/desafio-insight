import Repository from "./Repository";

const resource = "/user";

export default {
    // create(payload) {
    //     return Repository.post(`${resource}`, payload);
    // },
    // getAll() {
    //     return Repository.get(`${resource}`);
    // },
    // getOne(id) {
    //     return Repository.get(`${resource}/${id}`);
    // },
    // update(id, payload) {
    //     return Repository.put(`${resource}/${id}`, payload);
    // },
    // delete(id) {
    //     return Repository.delete(`${resource}`, id);
    // },
    getPapeis() {
        return Repository.get(`${resource}/papeis`);
    }
}