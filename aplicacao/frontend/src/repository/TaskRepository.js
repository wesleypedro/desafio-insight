import Repository from "./Repository";

const resource = "/task";

export default {
    create(payload) {
        return Repository.post(`${resource}`, payload);
    },
    getAll() {
        return Repository.get(`${resource}`);
    },
    getOne(id) {
        return Repository.get(`${resource}/${id}`);
    },
    update(id, payload) {
        return Repository.put(`${resource}/${id}`, payload);
    },
    delete(id) {
        return Repository.delete(`${resource}/${id}`);
    },
    progress(id) {
        return Repository.put(`${resource}/progress/${id}`);
    },
    finished(id) {
        return Repository.put(`${resource}/finished/${id}`);
    }
}