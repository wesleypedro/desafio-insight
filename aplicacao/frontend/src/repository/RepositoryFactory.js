import taskRepository from "./TaskRepository";
import serverRepository from "./ServerRepository";
import userRepository from "./UserRepository";

const repositories = {
    task: taskRepository,
    server: serverRepository,
    user: userRepository,
};

export const RepositoryFactory = {
    get: name => repositories[name]
};
