import Vue from 'vue';
import Vuex from 'vuex';

import app from './modules/app';
import auth from './modules/auth';
import task from './modules/task';
// import admin from './modules/administrator';
import server from './modules/server';
import user from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store ({
    modules: {
        app: app,
        auth: auth,
        tasks: task,
        // admin: admin,
        server: server,
        user: user,
    }
});