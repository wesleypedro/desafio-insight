// import router from "../../router";
import {RepositoryFactory} from "../../repository/RepositoryFactory";

let serverRepository = RepositoryFactory.get("server");

const state = {
    servers: [],
};

const actions = {
  getAll({commit}) {
    serverRepository.getAll()
      .then((response) => {
        commit('SET_SERVERS', response.data);
      })
      .catch(error => {
        console.log(error);
        console.error;
        commit('CLEAR_SERVER');
      });
  },
};

const getters = {
    GET_SERVERS(state) {
        return state.servers;
    }
};

const mutations = {
    SET_SERVERS: (state, servers) => {
        state.servers = servers;
    },

    CLEAR_SERVERS: (state) => {
        state.servers = [];
    }
};

export default  {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};