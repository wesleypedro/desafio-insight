import router from "../../router";
import {RepositoryFactory} from "../../repository/RepositoryFactory"

let taskRepository = RepositoryFactory.get("task");

const state = {
  priority: [
    {
      text: "Prioridade baixa",
      value: "BAIXA",
    },
    {
      text: "Prioridade média",
      value: "MEDIA",
    },
    {
      text: "Prioridade alta",
      value: "ALTA",
    },
  ],

  status: [
    {
      text: "Em aberto",
      value: "ABERTA",
    },
    {
      text: "Em andamento",
      value: "ANDAMENTO",
    },
    {
      text: "Finalizada",
      value: "FINALIZADA",
    },
  ],

  tasks: [],

  default_task: {
    name: "",
    deadline: new Date().toISOString().substr(0, 10),
    responsible: {
      id: -1,
    },
    taskPriority: "",
    description: "",
  },
}

const actions = {
  putTasks({commit}, tasks) {
    commit('SET_TASKS', tasks);
  },

  createTask({commit}, task) {
    taskRepository.create(task)
      .then((response) => {
        commit('CREATE_TASK', response.data);
        // router.push("/create");
        router.push("/create")
        // this.$router.push("/create")
        alert("Salvo com sucesso!");
      })
      .catch(error => {
        console.log(error);
        console.error;
        alert("Erro ao salvar!");
      });
  },

  getAll({commit}) {
    taskRepository.getAll()
      .then((response) =>  {
        commit('SET_TASKS', response.data);
      })
      .catch(error => {
        console.log(error);
      })
  },

  update({commit}, task) {
    taskRepository.update(task.id, task)
      .then((response) => {
        commit('UPDATE_TASK', response.data);
        alert("Atualizado com sucesso!");
      })
      .catch(error => {
        console.log(error);
        alert("Erro ao atualizar tarefa!");
      })
  },

  getOne(id) {
    taskRepository.getOne(id)
      .then((response) =>  {
        return response.data;
      })
      .catch(error => {
        console.log(error);
      })
  },

  delete({commit}, id) {
    taskRepository.delete(id)
      .then((response) =>  {
        commit('DELETE_TASK', response.data)
        // router.push('/list')
        alert("Removido com sucesso!");
      })
      .catch(error => {
        console.log(error);
        alert("Erro ao remover!");
      })
  },

  progress({commit}, id) {
    taskRepository.progress(id)
      .then((response) => {
        commit('UPDATE_TASK', response.data);
        // router.push("/list");
        alert("Atualizado com sucesso!");
      })
      .then(error => {
        console.error;
        console.log(error);
        // alert("Erro ao atualizar!");
      });
  },

  finished({commit}, id) {
    taskRepository.finished(id)
      .then((response) => {
        commit('UPDATE_TASK', response.data);
        // router.push("/list");
        alert("Atualizado com sucesso!");
      })
      .then(error => {
        console.error;
        console.log(error);
        // alert("Erro ao atualizar!");
      });
  },
}

const getters = {
  GET_TASKS(state) {
    return state.tasks;
  },
  GET_DEFAULT_TASK(state) {
    return state.default_task;
  },
  GET_STATUS(state) {
    return state.status;
  },
  GET_PRIORITY(state) {
    return state.priority;
  },
}

const mutations = {
  UPDATE_TASK: (state, task) => {
    let index = state.tasks.findIndex(x => x.id === task.id);
    state.tasks[index] = task;
  },
  CREATE_TASK: (state, task) => {
    state.tasks.push(task);
  },
  DELETE_TASK: (state, task) => {
    let tasks_ = state.tasks.filter(x => x.id != task.id);
    state.tasks = tasks_;
  },
  SET_TASKS: (state, tasks) => {
    state.tasks = tasks;
  },
  CLEAR_TASK: (state) => {
    state.task = null;
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};