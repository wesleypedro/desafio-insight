// import router from "../../router";
// import {RepositoryFactory} from "../../repository/RepositoryFactory";

// let adminRepository = RepositoryFactory.get("admin");

const state = {};

const actions = {};

const getters = {};

const mutations = {};

export default  {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
};