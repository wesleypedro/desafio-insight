// import router from "../../router";
import { RepositoryFactory } from "../../repository/RepositoryFactory";

let userRepository = RepositoryFactory.get("user");

const state = {
  papeis: [],
};

const actions = {
  getPapeis({ commit }) {
    userRepository.getPapeis()
      .then((response) => {
        commit('SET_PAPEIS', response.data);
      })
      .catch(error => {
        console.log(error);
        console.error;
        commit('CLEAR_PAPEIS');
      });
  },
};

const getters = {
  GET_PAPEIS(state) {
    return state.papeis;
  }
};

const mutations = {
  SET_PAPEIS: (state, papeis) => {
    state.papeis = papeis;
  },

  CLEAR_PAPEIS: (state) => {
    state.papeis = [];
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};