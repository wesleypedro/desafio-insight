import Home from '../views/Home';
import About from '../views/About';

import LoginUser from '../views/user/LoginUser';
import CreateUser from '../views/user/CreateUser';

import CreateTask from '../views/task/CreateTask';
import ListTasks from '../views/task/ListTasks';

export default [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/about',
    name: 'About',
    component: About,
  },

  {
    path: '/login',
    name: 'Login',
    component: LoginUser,
    props: true,
    meta: {
      public: true,
    },
  },
  {
    path: '/register',
    name: 'Register',
    component: CreateUser,
    props: true,
    meta: {
      public: true,
    },
  },

  {
    path: '/logout',
    name: 'Logout',
    meta: {
      breadcrumb: true,
    },
  },

  {
    path: '/create',
    name: 'CreateTask',
    component: CreateTask,
    props: true,
  },
  {
    path: '/list',
    name: 'ListTasks',
    component: ListTasks,
    props: true,
  },
];